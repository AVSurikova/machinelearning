package org.oak.uml.data;

import java.util.Objects;

public class Tank {
    private final String nation;
    private final String type;
    private final String vehicle;
    private final double lvl;
    private final double hp;
    private final double maxSpeed;
    private final double maxSpeedBack;
    private final double coolDown;
    private final double aimTime;
    private final double accuracy;
    private final double damage;

    public Tank(String nation, String type, String vehicle, double lvl, double hp, double maxSpeed, double maxSpeedBack,
                double coolDown, double aimTime, double accuracy, double damage) {
        this.nation = nation;
        this.type = type;
        this.vehicle = vehicle;
        this.lvl = lvl;
        this.hp = hp;
        this.maxSpeed = maxSpeed;
        this.maxSpeedBack = maxSpeedBack;
        this.coolDown = coolDown;
        this.aimTime = aimTime;
        this.accuracy = accuracy;
        this.damage = damage;
    }

    public String getNation() {
        return nation;
    }

    public String getType() {
        return type;
    }

    public String getVehicle() {
        return vehicle;
    }

    public double getLvl() {
        return lvl;
    }

    public double getHp() {
        return hp;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public double getMaxSpeedBack() {
        return maxSpeedBack;
    }

    public double getCoolDown() {
        return coolDown;
    }

    public double getAimTime() {
        return aimTime;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public double getDamage() {
        return damage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tank tank = (Tank) o;
        return Double.compare(tank.lvl, lvl) == 0 &&
                Double.compare(tank.hp, hp) == 0 &&
                Double.compare(tank.maxSpeed, maxSpeed) == 0 &&
                Double.compare(tank.maxSpeedBack, maxSpeedBack) == 0 &&
                Double.compare(tank.coolDown, coolDown) == 0 &&
                Double.compare(tank.aimTime, aimTime) == 0 &&
                Double.compare(tank.accuracy, accuracy) == 0 &&
                Double.compare(tank.damage, damage) == 0 &&
                Objects.equals(nation, tank.nation) &&
                Objects.equals(type, tank.type) &&
                Objects.equals(vehicle, tank.vehicle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nation, type, vehicle, lvl, hp, maxSpeed, maxSpeedBack, coolDown, aimTime, accuracy, damage);
    }

    @Override
    public String toString() {
        return "{" +
                "nation='" + nation + '\'' +
                ", type='" + type + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", lvl=" + lvl +
                ", hp=" + hp +
                ", maxSpeed=" + maxSpeed +
                ", maxSpeedBack=" + maxSpeedBack +
                ", coolDown=" + coolDown +
                ", aimTime=" + aimTime +
                ", accuracy=" + accuracy +
                ", damage=" + damage +
                '}';
    }
}