package org.oak.uml.data;

import org.oak.uml.algorithm.model.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomPointsGenerator {
    public static List<Point> generatePoints(int count, int size) {
        List<Point> points = new ArrayList<>();

        Random random = new Random();
        for (int i = 0; i < count; i++) {
            double[] array = new double[size];
            for (int j = 0; j < array.length; j++) {
                array[j] = random.nextInt(201) - 100;
            }
            points.add(new Point(array));
        }

        return points;
    }
}
