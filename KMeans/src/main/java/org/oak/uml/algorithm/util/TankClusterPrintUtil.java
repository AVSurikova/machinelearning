package org.oak.uml.algorithm.util;

import org.oak.uml.algorithm.model.Cluster;
import org.oak.uml.algorithm.model.Point;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class TankClusterPrintUtil {

    public static void printClusters(List<Cluster> clusters) {
        System.out.println("\nClusters count: " + clusters.size());
        System.out.println("Total points: " + clusters.stream().mapToLong(c -> c.getPoints().size()).sum());
        clusters.forEach(TankClusterPrintUtil::printCluster);
    }

    public static void printCluster(Cluster cluster) {
        Set<Point> points = cluster.getPoints();

        System.out.println("\nCluster size: " + points.size());
        points.stream().collect(groupingBy(p -> p.getDescription().split(":")[0], Collectors.toList())).forEach((k, v) -> {
            System.out.println("For nation:" + k + ", got " + v.size());
            System.out.println(v);
        });
        System.out.println();
        points.stream().collect(groupingBy(p -> p.getDescription().split(":")[1], Collectors.toList())).forEach((k, v) -> {
            System.out.println("For class:" + k + ", got " + v.size());
            System.out.println(v);
        });
        System.out.println();
        points.stream().collect(groupingBy(p -> p.getCoordinates()[0] + "", Collectors.toList())).forEach((k, v) -> {
            System.out.println("For level:" + k + ", got " + v.size());
            System.out.println(v);
        });
    }
}
