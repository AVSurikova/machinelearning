package org.oak.uml.algorithm.util;

import org.oak.uml.algorithm.model.Point;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NormUtil {

    public static List<Point> minMaxNorm(List<Point> points) {
        double[] min = Arrays.copyOf(points.get(0).getCoordinates(), points.get(0).getCoordinates().length);
        double[] max = Arrays.copyOf(points.get(0).getCoordinates(), points.get(0).getCoordinates().length);
        for (Point p : points) {
            double[] crd = p.getCoordinates();
            for (int i = 0; i < crd.length; ++i) {
                if (crd[i] < min[i]) {
                    min[i] = crd[i];
                } else if (crd[i] > max[i]) {
                    max[i] = crd[i];
                }
            }
        }

        double[] slope = new double[min.length];
        for (int i = 0; i < slope.length; ++i) {
            slope[i] = 1 / (max[i] - min[i]);
        }

        return points.parallelStream().map(p -> {
            double[] oldCrd = p.getCoordinates();
            double[] newCrd = new double[oldCrd.length];
            for (int i = 0; i < oldCrd.length; ++i) {
                newCrd[i] = slope[i] * (oldCrd[i] - min[i]);
            }
            return new Point(p.getDescription(), newCrd);
        }).collect(Collectors.toList());
    }

    public static List<Point> euclidNorm(List<Point> points) {
        return points.stream().map(p -> {
            double length = Math.sqrt(Arrays.stream(p.getCoordinates()).map(a -> Math.pow(a, 2)).sum());
            return new Point(p.getDescription(), Arrays.stream(p.getCoordinates()).map(a -> a / length).toArray());
        }).collect(Collectors.toList());
    }
}
