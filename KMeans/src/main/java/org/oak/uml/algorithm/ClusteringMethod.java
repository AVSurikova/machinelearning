package org.oak.uml.algorithm;

import org.oak.uml.algorithm.model.Cluster;

import java.util.List;

public interface ClusteringMethod {
    List<Cluster> clusters();
}
