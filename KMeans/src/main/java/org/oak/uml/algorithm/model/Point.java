package org.oak.uml.algorithm.model;

import java.util.Arrays;
import java.util.Objects;

public class Point {
    private final String description;
    private final double[] coordinates;

    public Point(double... coordinates) {
        this("", coordinates);
    }

    public Point(String description, double... coordinates) {
        this.coordinates = coordinates;
        this.description = description;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Arrays.equals(coordinates, point.coordinates) &&
                Objects.equals(description, point.description);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(description);
        result = 31 * result + Arrays.hashCode(coordinates);
        return result;
    }


    @Override
    public String toString() {
        return "{" +
                "description='" + description + '\'' +
                ", coordinates=" + Arrays.toString(coordinates) +
                '}';
    }
}
