package org.oak.uml.algorithm;

import org.oak.uml.algorithm.model.Cluster;
import org.oak.uml.algorithm.model.Point;
import org.oak.uml.algorithm.util.DistanceMeasure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KMeans implements ClusteringMethod {

    private int iterations;
    private int clustersNumber;
    private boolean randomize;

    private final List<Point> points;
    private final DistanceMeasure measure;

    public KMeans(List<Point> points, DistanceMeasure measure, int clustersNumber, int iterations) {
        this(points, measure, clustersNumber, iterations, false);
    }

    public KMeans(List<Point> points, DistanceMeasure measure, int clustersNumber, int iterations, boolean randomize) {
        this.points = points;
        this.measure = measure;
        this.clustersNumber = clustersNumber;
        this.iterations = iterations;
        this.randomize = randomize;
    }

    public List<Cluster> clusters() {
        List<Cluster> clusters = initClusters(points, clustersNumber, randomize);

        boolean updated;
        do {
            updated = updateClusters(clusters);
        } while (iterations-- > 0 && updated);

        if (iterations != 0) {
            System.out.println("No movement detected, skipping last iterations: " + iterations);
        }

        return clusters;
    }

    public void setClustersNumber(int clustersNumber) {
        this.clustersNumber = clustersNumber;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public void setRandomize(boolean randomize) {
        this.randomize = randomize;
    }

    private List<Cluster> initClusters(List<Point> points, int count, boolean randomize) {
        if (randomize) {
            Collections.shuffle(points);
        }

        List<Cluster> clusters = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Cluster cluster = new Cluster();
            clusters.add(cluster);
            cluster.addPoint(points.get(i));
        }
        points.forEach(p -> measure.findClosest(p, clusters).addPoint(p));

        return clusters;
    }

    private boolean updateClusters(List<Cluster> clusters) {
        boolean updated = false;
        for (Cluster cluster : clusters) {
            List<Point> clusterPoints = new ArrayList<>(cluster.getPoints());
            for (Point p : clusterPoints) {
                Cluster newCluster = measure.findClosest(p, clusters);
                if (!newCluster.equals(cluster)) {
                    cluster.deletePoint(p);
                    newCluster.addPoint(p);
                    updated = true;
                }
            }
        }
        return updated;
    }
}
